package linkedLists;

import java.lang.reflect.Array;
import java.util.NoSuchElementException;

public class DLDHDTList<E> extends AbstractDLList<E> {
	private DNode<E> header, trailer; 
	private int length; 
	
	public DLDHDTList() { 
		// ADD CODE HERE to generate empty linked list of this type
		header= new DNode<E>();
		trailer=new DNode<E>(null, header, null);
		length=0;
	}
	
	public void addFirstNode(Node<E> nuevo) {
		addNodeAfter(header, nuevo); 
	}
	
	public void addLastNode(Node<E> nuevo) { 
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nBefore = trailer.getPrev();  
		nBefore.setNext(dnuevo); 
		trailer.setPrev(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext(trailer); 
		length++; 
	}

	public void addNodeAfter(Node<E> target, Node<E> nuevo) {
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nBefore = (DNode<E>) target; 
		DNode<E> nAfter = nBefore.getNext(); 
		nBefore.setNext(dnuevo); 
		nAfter.setPrev(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext(nAfter); 
		length++; 
	}

	public void addNodeBefore(Node<E> target, Node<E> nuevo) {
		// ADD CODE HERE
		DNode<E> dnuevo = (DNode<E>) nuevo;
		DNode<E> nAfter = (DNode<E>) target;
		DNode<E> nBefore = nAfter.getPrev();
		nBefore.setNext(dnuevo);
		nAfter.setPrev(dnuevo); 
		dnuevo.setPrev(nBefore); 
		dnuevo.setNext(nAfter); 
		length++; 
	}

	public Node<E> createNewNode() {
		return new DNode<E>();
	}

	public Node<E> getFirstNode() throws NoSuchElementException {
		if (length == 0) 
			throw new NoSuchElementException("List is empty."); 
		return header.getNext();
	}

	public Node<E> getLastNode() throws NoSuchElementException {
		if (length == 0) 
			throw new NoSuchElementException("List is empty."); 
		return trailer.getPrev();
	}

	public Node<E> getNodeAfter(Node<E> target)
			throws NoSuchElementException {
		DNode<E> aNode = (DNode<E>) target; 
		if (aNode.getNext() == trailer)  
			throw new NoSuchElementException("Next node does not exist. Target is the last node."); 
		
		return (Node<E>) aNode.getNext();
	}

	public Node<E> getNodeBefore(Node<E> target)
			throws NoSuchElementException {
		// ADD CODE HERE AND MODIFY RETURN ACCORDINGLY
		DNode<E> prev = ((DNode<E>) target);
		if(prev.getPrev()==header) 
			throw new NoSuchElementException("Previous node does not exist. target is the first node."); 

		return(Node<E>) prev.getPrev(); 
	}

	public int length() {
		return length;
	}
	
	public Node<E> findNode(int index) throws NoSuchElementException{
		DNode<E> target=(DNode<E>) header;
		if(length==0||index<0||index>length){
			throw new NoSuchElementException("invalid index. Either the list is empty or the given"
					+ " index is invalid for the list.");
		}
		for(int i=0; i<index;i++){
			target=target.getNext();
		}
		return target;
	}

	public void removeNode(Node<E> target) {
		DNode<E> dTarget = (DNode<E>) target;
		DNode<E> nodoAnterior = dTarget.getPrev();
		DNode<E> nodoDesps = dTarget.getNext();
		nodoAnterior.setNext(nodoDesps);
		nodoDesps.setPrev(nodoAnterior);
		dTarget.clean();
		length --;
	}
	
	public Object[] toArray() {
		Object[] array = new Object[this.length()]; 
	    DNode<E> target= (DNode<E>) header;
	    int c=0;
		while(target.getNext()!=trailer){
			array[c]=target.getElement();
			c++;
		}
		    return array; 
	}


	public <T1> T1[] toArray(T1[] array) {
		if (array.length < this.length()) { 
	        // if arr.length < this.size, allocate a new array of the same 
	    	// type as arr (components of the new array are set to be of equal
	    	// runtime type as components of the original array arr) 
	    	// and big enough to hold all the elements in this set. For 
	    	// this, we need to use the Array class....
	    	array = (T1[]) Array.newInstance(array.getClass().getComponentType(), this.length());
	    } else if (array.length > this.length()) 
	    	// Set to null all those positions of arr that won't be used. 
	    	for (int j=this.length(); j< array.length; j++) 
	    		array[j] = null;
	    
	    
	    int c=0;
	    DNode<E> target= (DNode<E>) header;
	    while(target.getNext()!=trailer){
	    	array[c]=(T1) target.getElement();
	    }
	    return array; 
	}
	
	/**
	 * Prepares every node so that the garbage collector can free 
	 * its memory space, at least from the point of view of the
	 * list. This method is supposed to be used whenever the 
	 * list object is not going to be used anymore. Removes all
	 * physical nodes (data nodes and control nodes, if any)
	 * from the linked list
	 */
	private void destroy() {
		while (header != null) { 
			DNode<E> nnode = header.getNext(); 
			header.clean(); 
			header = nnode; 
		}
	}
	
	/**
	 * The execution of this method removes all the data nodes from
	 * the current instance of the list, leaving it as a valid empty
	 * doubly linked list with dummy header and dummy trailer nodes. 
	 */
	public void makeEmpty() { 
		// TODO
	}
		
	protected void finalize() throws Throwable {
	    try {
			this.destroy(); 
	    } finally {
	        super.finalize();
	    }
	
	}
	

}
