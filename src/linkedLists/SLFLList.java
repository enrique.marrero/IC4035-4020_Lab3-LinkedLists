package linkedLists;
/**
 * Singly linked list with references to its first and its
 * last node. 
 * 
 * @author pirvos
 *
 */

import java.lang.reflect.Array;
import java.util.NoSuchElementException;

import linkedLists.LinkedList;
import linkedLists.AbstractDLList.DNode;

public class SLFLList<E> extends SLList<E> {
	private SNode<E> first, last;   // reference to the first node and to the last node
	int length; 
	
	public SLFLList() {       // to create an empty list instance
		first = last = null; 
		length = 0; 
	}
	
	
	public void addFirstNode(Node<E> nuevo) {
		// TODO Auto-generated method stub
		((SNode<E>)nuevo).setNext(first);
		first=(SNode<E>) nuevo;
		length++;
		
	}

	public void addNodeAfter(Node<E> target, Node<E> nuevo) {
		// TODO Auto-generated method stub
		((SNode<E>)nuevo).setNext(((SNode<E>)target).getNext());
		((SNode<E>)target).setNext((SNode<E>)nuevo); 
		length++;
		
				
	}

	public void addNodeBefore(Node<E> target, Node<E> nuevo) {
		// TODO Auto-generated method stub
		if(target==first)
			this.addFirstNode(nuevo);
		else{
			this.addNodeAfter(getNodeBefore(target), nuevo);
		}
	}

	public Node<E> getFirstNode() throws NoSuchElementException {
		// TODO Auto-generated method stub
		if(first==null){
			throw new NoSuchElementException("List is empty.");
		}
		return first;
	}

	public Node<E> getLastNode() throws NoSuchElementException {
		// TODO Auto-generated method stub
		if(last==null){
			throw new NoSuchElementException("List is empty.");
		}
		return last;
	}

	public Node<E> getNodeAfter(Node<E> target) throws NoSuchElementException {
		// TODO Auto-generated method stub
		SNode<E> aNode=((SNode<E>)target).getNext(); 
		if(aNode==null) 
			throw new NoSuchElementException("Next node does not exist. Target is the last node.");
		return aNode;
	}

	public Node<E> getNodeBefore(Node<E> target)
			throws NoSuchElementException {
		// TODO Auto-generated method stub
		if(target==first)
			throw new NoSuchElementException("Previous node does not exist. Target is the first node.");
		SNode<E> prev=first;
		while(prev.getNext()!=target)
			prev=prev.getNext();
		return prev;
	}
	
	public Object[] toArray() {
		Object[] array = new Object[this.length()]; 
	    SNode<E> target= (SNode<E>) first;
	    int c=0;
		while(target.getNext()!=last){
			array[c]=target.getElement();
			c++;
		}
		    return array; 
	}


	public <T1> T1[] toArray(T1[] array) {
		if (array.length < this.length()) { 
	        // if arr.length < this.size, allocate a new array of the same 
	    	// type as arr (components of the new array are set to be of equal
	    	// runtime type as components of the original array arr) 
	    	// and big enough to hold all the elements in this set. For 
	    	// this, we need to use the Array class....
	    	array = (T1[]) Array.newInstance(array.getClass().getComponentType(), this.length());
	    } else if (array.length > this.length()) 
	    	// Set to null all those positions of arr that won't be used. 
	    	for (int j=this.length(); j< array.length; j++) 
	    		array[j] = null;
	    
	    
	    int c=0;
	    SNode<E> target= (SNode<E>) first;
	    while(target.getNext()!=last){
	    	array[c]=(T1) target.getElement();
	    }
	    return array; 
	}
	
	public Node<E> findNode(int index) throws NoSuchElementException{
		SNode<E> target=(SNode<E>) first;
		if(length==0||index<0||index>length){
			throw new NoSuchElementException("invalid index. Either the list is empty or the given"
					+ " index is invalid for the list.");
		}
		for(int i=0; i<index;i++){
			target=target.getNext();
		}
		return target;
	}

	public int length() {
		// TODO Auto-generated method stub
		return this.length;
	}

	public void removeNode(Node<E> target) {
		// TODO Auto-generated method stub
		if(target==first){
			SNode<E> ntr= first;
			first=first.getNext();
			ntr.setNext(null);
		}
		else{
			SNode<E> prevNode= (SNode<E>) getNodeBefore(target);
			prevNode.setNext(((SNode<E>)target).getNext());
		}
		
	}
	
	public Node<E> createNewNode() {
		return new SNode<E>();
	}

}
