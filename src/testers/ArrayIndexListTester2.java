package testers;

import java.util.Random;

import linkedLists.DLDHDTList;
import linkedLists.Node;
import linkedLists.SLFLList;

public class ArrayIndexListTester2 {

	public static void main(String[] args) { 
		Random r = new Random(5);   // fixing seed of random sequence to 5
		SLFLList<Integer> list= new SLFLList<>();
	}
	
	private static void showList(SLFLList<Integer> list, String msg) { 
		System.out.println("Content of list " + msg); 
		
		for (int i=0; i<list.length(); i++) 
			System.out.print(list.findNode(i).getElement() + " "); 

		System.out.println(); 
		System.out.println("-----------------------------------------"); 
		
	}
	
	private static void showArray(Object[] arr, String msg) { 
		System.out.println("Content of array " + msg); 
		
		for (int i=0; i<arr.length; i++) 
			System.out.print(arr[i] + " "); 
		
		System.out.println(); 
		System.out.println("-----------------------------------------"); 
		
	}
}
